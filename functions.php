<?php


//return stdClass or assocArray
function getPosts($return_assoc = 0)
{

    $posts = json_decode(file_get_contents(POST_DB),$return_assoc);
    return $posts;
}



// $post->{title,author,content}
function savePost(stdClass $post):bool
{
    $posts = getPosts(1);
    $posts[]= (array)$post;
    $posts_js = json_encode($posts);
    file_put_contents(POST_DB,$posts_js);
    return true;

}


//check is_admin return bool
function isAdmin( $user_name = 0 ) : bool
{

    return 1;
}

//check isLoggedIn return bool
function isLoggedIn() : bool
{

    return 1;
}


//create auto url ,return string url
function siteUrl( $uri ="" ) : string
{

    return BASE_URL . $uri ;
}


//create auto url ,return string url
function panelUrl( $uri ="" ) : string
{

    return BASE_URL . "panel/" . $uri ;
}